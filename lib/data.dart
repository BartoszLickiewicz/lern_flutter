import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:shared_preferences/shared_preferences.dart';

class Data extends StatefulWidget {
  const Data({super.key});

  @override
  State<Data> createState() => _DataState();
}

class _DataState extends State<Data> {
  String _text = '';

  @override
  void initState() {
    super.initState();
    _loadText();
  }

  void _loadText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _text = prefs.getString('my_text') ?? '';
    });
  }

  void _saveText(String text) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('my_text', text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          TextField(
            decoration: const InputDecoration(
              hintText: 'Enter some text',
            ),
            onChanged: (text) {
              _saveText(text);
            },
          ),
          const SizedBox(height: 20),
          Text('Saved Text: $_text'),
        ],
      ),
    );
  }
}
