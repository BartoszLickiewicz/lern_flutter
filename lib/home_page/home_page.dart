import 'package:flutter/material.dart';
import 'package:flutter_application_2/home_page/page_1.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              //decoration: BoxDecoration(border: Border.all()),
              margin: const EdgeInsets.all(10.0),
              child: Image.asset('images/gears.jpg'),
            ),
            Container(
              color: Colors.orange,
              margin: const EdgeInsets.all(10.0),
              padding: const EdgeInsets.all(10.0),
              width: double.infinity,
              child: const Center(
                child: Text(
                  'Welcome to My page',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Container(
              // color: Colors.orange,
              decoration: BoxDecoration(border: Border.all()),
              margin: const EdgeInsets.all(10.0),
              padding: const EdgeInsets.all(10.0),
              width: double.infinity,
              child: const Center(
                child: Text(
                  'Hello and welcome to our test version of the application! Were thrilled to have you onboard as one of our early users. As we continue to develop and improve the app, your feedback and suggestions will be invaluable in helping us create the best possible experience for you. Thank you for taking the time to try out our application, and we look forward to hearing your thoughts!',
                  style: TextStyle(
                      // fontWeight: FontWeight.bold,
                      ),
                ),
              ),
            ),
            SizedBox(
              height: 100,
              width: 100,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(context, page_1());
                  //Navigator.push(
                  // context,
                  // MaterialPageRoute(builder: (context) => const Data()),
                  //);
                },
                child: const Icon(
                  Icons.settings,
                  size: 60,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
