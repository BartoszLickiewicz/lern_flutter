import 'package:flutter/material.dart';
import '../models/quote.dart';

List<Quote> quotes = [
  Quote(
      author: 'Mahatma Gandhi',
      text: 'Be the change that you wish to see in the world.'),
  Quote(author: 'J.R.R. Tolkien', text: 'Not all those who wander are lost..'),
  Quote(
      author: 'Ralph Waldo Emerson',
      text:
          'To be yourself in a world that is constantly trying to make you something else is the greatest accomplishment..'),
  Quote(
      author: 'Robert Frost',
      text:
          "In three words I can sum up everything I've learned about life: it goes on."),
  Quote(
      author: 'Steve Jobs',
      text: 'The only way to do great work is to love what you do.'),
];
int X = 1;

class page_1 extends MaterialPageRoute<void> {
  page_1()
      : super(
          builder: (BuildContext context) {
            return Scaffold(
              appBar: AppBar(
                title: const Text('Learn Flutter'),
              ),
              body: Center(
                child: Column(
                  children: [
                    Container(
                      color: Colors.orange,
                      margin: const EdgeInsets.all(10.0),
                      padding: const EdgeInsets.all(10.0),
                      width: double.infinity,
                      child: const Center(
                        child: Text(
                          'Here are quotes from famous people',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    Column(
                      children: quotes
                          .map((quote) => Text(
                                '${quote.text} \n- ${quote.author}\n',
                                textAlign: TextAlign.center,
                              ))
                          .toList(),
                    ),
                  ],
                ),
              ),
            );
          },
        );
}
