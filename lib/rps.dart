import 'package:flutter/material.dart';
import 'dart:math';

class Rps extends StatefulWidget {
  const Rps({super.key});

  @override
  State<Rps> createState() => _RpsState();
}

class _RpsState extends State<Rps> {
  void fade(X, colour, Y) async {
    setState(() {
      colour.value = Y;
      X.value = 1.0;
    });
    await Future.delayed(const Duration(milliseconds: 300));
    setState(() {
      X.value = 0.0;
    });

    //await Future.delayed(const Duration(seconds: 1));
    //X = 0.0;
  }

  List<IconData> iconList = [
    Icons.square,
    Icons.circle,
    Icons.cut,
  ];

  List<Color> colorList = [
    Colors.red[200]!,
    Colors.orange[200]!,
    Colors.green[200]!,
  ];

  Reference<double> X = Reference<double>(0.0);
  Reference<int> tieCounter = Reference(0);
  Reference<int> winCounter = Reference(0);
  Reference<int> lossCounter = Reference(0);
  Reference<int> mainIcon = Reference(0);
  Reference<int> color = Reference(0);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: [
        Container(
          color: Colors.amber[800],
          margin: const EdgeInsets.all(10.0),
          padding: const EdgeInsets.all(10.0),
          width: double.infinity,
          child: const Center(
            child: Text(
              'Rock Paper Scissors Game',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Stack(children: [
          AnimatedOpacity(
            opacity: X.value,
            duration: const Duration(milliseconds: 150),
            child: Container(
              width: 150,
              height: 150,
              decoration: BoxDecoration(
                color: colorList[color.value],
                borderRadius:
                    BorderRadius.circular(10), // set the border radius
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5), // set the shadow color
                    spreadRadius: 5, // set the spread radius
                    blurRadius: 7, // set the blur radius
                    offset: const Offset(0, 3), // set the shadow offset
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 25,
            left: 25,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey[400],
                borderRadius:
                    BorderRadius.circular(10), // set the border radius
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5), // set the shadow color
                    spreadRadius: 5, // set the spread radius
                    blurRadius: 7, // set the blur radius
                    offset: const Offset(0, 3), // set the shadow offset
                  ),
                ],
              ),
              child: Icon(
                //main icon
                iconList[mainIcon.value],
                color: Colors.red,
                size: 100.0,
              ),
            ),
          ),
        ]),
        Container(
          color: Colors.grey,
          margin: const EdgeInsets.all(10.0),
          padding: const EdgeInsets.all(10.0),
          width: double.infinity,
          child: const Center(
            child: Text(
              'Chose item',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              onPressed: () {
                setState(() {
                  run(tieCounter, winCounter, lossCounter, mainIcon, 0);
                });
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                    Colors.grey[400]), // Set the background color
                padding: MaterialStateProperty.all(
                    const EdgeInsets.all(20)), // Set the padding
              ),
              child: const Icon(
                Icons.square,
                size: 50,
              ),
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  run(tieCounter, winCounter, lossCounter, mainIcon, 1);
                });
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                    Colors.grey[400]), // Set the background color
                padding: MaterialStateProperty.all(
                    const EdgeInsets.all(20)), // Set the padding
              ),
              child: const Icon(
                Icons.circle,
                size: 50,
              ),
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  run(tieCounter, winCounter, lossCounter, mainIcon, 2);
                });
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                    Colors.grey[400]), // Set the background color
                padding: MaterialStateProperty.all(
                    const EdgeInsets.all(20)), // Set the padding
              ),
              child: const Icon(
                Icons.cut,
                size: 50,
              ),
            ),
          ],
        ),
        Container(
          color: Colors.grey,
          margin: const EdgeInsets.all(10.0),
          padding: const EdgeInsets.all(10.0),
          width: double.infinity,
          child: const Center(
            child: Text(
              'Your Score',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              'Won: ${winCounter.value}',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              'Draw: ${tieCounter.value}',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              'Lost: ${lossCounter.value}',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ]),
    );
  }

  void run(tieCounter, winCounter, lossCounter, mainIcon, int answer) {
    Random random = Random();
    if (mainIcon.value == answer) {
      fade(X, color, 1);
      tieCounter.value++;
      debugPrint("${tieCounter.value}");
    } else {
      if (answer == 0) {
        if (mainIcon.value == 1) {
          fade(X, color, 2);
          winCounter.value++;
        } else {
          fade(X, color, 0);
          lossCounter.value++;
        }
      }
      if (answer == 1) {
        if (mainIcon.value == 2) {
          fade(X, color, 2);
          winCounter.value++;
        } else {
          fade(X, color, 0);
          lossCounter.value++;
        }
      }
      if (answer == 2) {
        if (mainIcon.value == 0) {
          fade(X, color, 2);
          winCounter.value++;
        } else {
          fade(X, color, 0);
          lossCounter.value++;
        }
      }
    }

    mainIcon.value = random.nextInt(3);
  }
}

class Reference<T> {
  T value;
  Reference(this.value);
}

void increment(ref) {
  ref.value++; // increment the value by 1
}
