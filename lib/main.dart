import 'package:flutter/material.dart';

import 'data.dart';
import 'home_page/home_page.dart';
import 'weather.dart';
import 'quotes.dart';
import 'rps.dart';
import 'tiles.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  static const Color black = Color(0xFF000000);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.pink),
      home: const RootPage(),
    );
  }
}

class RootPage extends StatefulWidget {
  const RootPage({super.key});

  @override
  State<RootPage> createState() => _RootPageState();
}

class _RootPageState extends State<RootPage>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
  }

  int currentPage = 1;
  List<Widget> pages = const [
    HomePage(),
    Tiles(),
    Rps(),
    Weather(),
    Data(),
    AnimatedIconsExample(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('MyApp'),
      ),
      body: pages[currentPage],
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        color: Theme.of(context).primaryColor,
        child: Row(
          //mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            // Bottom that pops up from the bottom of the screen.
            IconButton(
                icon: const Icon(Icons.square_sharp),
                onPressed: () {
                  setState(() {
                    currentPage = 1;
                  });
                }),
            IconButton(
                icon: const Icon(Icons.gamepad),
                onPressed: () {
                  setState(() {
                    currentPage = 2;
                  });
                }),
            const SizedBox(),
            IconButton(
              icon: const Icon(Icons.cloudy_snowing),
              onPressed: () {
                setState(() {
                  currentPage = 3;
                });
              },
            ),
            IconButton(
              icon: const Icon(Icons.perm_contact_cal),
              onPressed: () {
                setState(() {
                  currentPage = 4;
                });
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            if (_animationController.isCompleted) {
              currentPage = 0;
              _animationController.reverse();
            } else {
              currentPage = 5;
              _animationController.forward();
            }
          });
        },
        child: AnimatedIcon(
          progress: _animationController,
          icon: AnimatedIcons.home_menu,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
