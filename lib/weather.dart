import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

const weatherApiUrl = 'https://api.openweathermap.org/data/2.5/weather';

class NetworkData {
  NetworkData(this.url);
  final String url;

  /// get geographical coordinates from open weather API call
  Future getData(context) async {
    http.Response response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      String data = response.body;
      return jsonDecode(data);
    } else {
      // ignore: avoid_print
      // _showModal(context, 'problem przy pobieraniu');
    }
  }
}

//https://api.openweathermap.org/data/2.5/weather?lat=0&lon=0&appid=a709f7e463ce6ebe8fb2bffff28d139b&units=metric
class WeatherModel {
  Future<dynamic> getLocationWeather(userInput, context) async {
    /// await for methods that return future
    //int? latitude;
    //int? longitide;
    String apiKey = 'a709f7e463ce6ebe8fb2bffff28d139b';

    /// Get location data
    ///&units=metric change the temperature metric
    NetworkData networkHelper =
        NetworkData('$weatherApiUrl?q=$userInput&appid=$apiKey&units=metric');
    var weatherData = await networkHelper.getData(context);
    return weatherData;
  }
}

//q={city name}
/// variable weatherData contain response from the API
/// to fetch data check the response to get the way the data structured

class Weather extends StatefulWidget {
  const Weather({super.key});

  @override
  State<Weather> createState() => _WeatherState();
}

class _WeatherState extends State<Weather> {
  // String userInput = myController.text;
  int temperature = 0;
  String condition = 'unknown';
  int humidity = 0;
  String country = 'unknown';
  String city = 'unknown';
  WeatherModel weatherModel = WeatherModel();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    getLocationData(userInput);
  }

  final myController = TextEditingController();
  String userInput = 'Wroclaw';

  getLocationData(userInput) async {
    var weatherData = await weatherModel.getLocationWeather(userInput, context);
    setState(() {
      if (weatherData != null) {
        condition = weatherData['weather'][0]['main'];
        humidity = weatherData['main']['humidity'];
        country = weatherData['sys']['country'];
        city = weatherData['name'];
        double temp = weatherData['main']['temp'];
        temperature = temp.toInt();
      } else {
        _showModal(context, 'There is no such city.');
      }
    });
  }

// // weatherData != null ? weatherData['weather'][0]['main'] : 'unknown';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                margin: const EdgeInsets.all(20.0),
                child: Text(
                  'Check the weather!',
                  style: CustomTextStyle.heading.copyWith(
                    //fontSize: 16,
                    fontWeight: FontWeight.bold,
                    //color: Colors.blue,
                  ),
                ),
              ),
              Divider(
                color: Colors.grey[400],
              ),
              Container(
                margin: const EdgeInsets.only(
                    top: 20, bottom: 20, right: 0, left: 20.0),
                child: Table(
                  columnWidths: const {
                    0: FlexColumnWidth(1),
                    1: FlexColumnWidth(1),
                  },
                  //border: TableBorder.all(),
                  children: [
                    TableRow(
                      children: [
                        const Text('City:', style: CustomTextStyle.heading),
                        Text(city, style: CustomTextStyle.heading),
                      ],
                    ),
                    TableRow(
                      children: [
                        const Text(
                          'Temperature:',
                          style: CustomTextStyle.heading,
                        ),
                        Text('$temperature°', style: CustomTextStyle.heading),
                      ],
                    ),
                    TableRow(
                      children: [
                        const Text('Condition:',
                            style: CustomTextStyle.heading),
                        Text(condition, style: CustomTextStyle.heading),
                      ],
                    ),
                    TableRow(
                      children: [
                        const Text('Humidity:', style: CustomTextStyle.heading),
                        Text('$humidity', style: CustomTextStyle.heading),
                      ],
                    ),
                    TableRow(
                      children: [
                        const Text('Country:', style: CustomTextStyle.heading),
                        Text(country, style: CustomTextStyle.heading),
                      ],
                    ),
                  ],
                ),
              ),
              Divider(
                color: Colors.grey.shade400,
              ),
              Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  margin:
                      const EdgeInsets.only(top: 20, right: 5.0, left: 20.0),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.5),
                    ),
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: TextField(
                    controller: myController, // assign controller to TextField
                    //onChanged: _updateText,
                    onSubmitted: (String newText) {
                      setState(() {
                        getLocationData(newText);
                      });
                    },
                    decoration: const InputDecoration(
                      hintText: 'Enter city name',
                      //labelText: 'Username',
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).viewInsets.bottom,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CustomTextStyle {
  static const heading = TextStyle(
    fontSize: 25,
    //fontWeight: FontWeight.bold,
    color: Colors.black,
    fontFamily: 'Spartan MB',
  );
}

void _showModal(context, String text) {
  showModalBottomSheet(
    context: context,
    builder: (BuildContext context) {
      return Container(
        height: 200,
        child: Center(
          child: Text(text, style: CustomTextStyle.heading),
        ),
      );
    },
  );
}
