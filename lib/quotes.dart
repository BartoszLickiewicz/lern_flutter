import 'package:flutter/material.dart';

const _kAllANimatedIcons = <String, AnimatedIconData>{
  'add_event': AnimatedIcons.add_event,
  'arrow_menu': AnimatedIcons.arrow_menu,
  'close_menu': AnimatedIcons.close_menu,
  'ellipsis_search': AnimatedIcons.ellipsis_search,
  'event_add': AnimatedIcons.event_add,
  'home_menu': AnimatedIcons.home_menu,
  'list_view': AnimatedIcons.list_view,
  'menu_arrow': AnimatedIcons.menu_arrow,
  'menu_close': AnimatedIcons.menu_close,
  'menu_home': AnimatedIcons.menu_home,
  'pause_play': AnimatedIcons.pause_play,
  'play_pause': AnimatedIcons.play_pause,
  'search_ellipsis': AnimatedIcons.search_ellipsis,
  'view_list': AnimatedIcons.view_list,
};

class AnimatedIconsExample extends StatefulWidget {
  const AnimatedIconsExample({super.key});

  @override
  State<AnimatedIconsExample> createState() => _AnimatedIconsExampleState();
}

class _AnimatedIconsExampleState extends State<AnimatedIconsExample>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  int isSwitch = 0;
  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          color: isSwitch == 0 ? Colors.orange : Colors.red,
          child: InkWell(
            onTap: () {
              setState(() {
                // ! Depending on the state, reverse or forward the animation.
                if (_animationController.isCompleted) {
                  isSwitch = 0;
                  _animationController.reverse();
                } else {
                  isSwitch = 1;
                  _animationController.forward();
                }
              });
            },
            child: AnimatedIcon(
              // ! Set the animation progress to our animation controller
              progress: _animationController,
              icon: AnimatedIcons.home_menu,
              size: 128,
            ),
          ),
        ),
      ],
    );
  }
}
