import 'package:flutter/material.dart';

class Tiles extends StatefulWidget {
  const Tiles({super.key});

  @override
  State<Tiles> createState() => _TilesState();
}

class Tile {
  String text;
  Color color;

  Tile({required this.text, required this.color});
}

//final List<String> items = List.generate(20, (index) => "$index");
List<Tile> items = List.generate(
  20,
  (index) => Tile(
    text: 'text $index',
    color: Colors.blue,
  ),
);
// ignore: unused_element
//Color _color = Colors.blue;

class _TilesState extends State<Tiles> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 500,
            child: GridView.builder(
              itemCount: items.length,
              padding: const EdgeInsets.all(20.0),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                crossAxisSpacing: 10.0,
                mainAxisSpacing: 10.0,
              ),
              itemBuilder: (BuildContext context, int index) {
                var item = items[index];
                return Container(
                  color: item.color,
                  child: ListTile(
                    title: Text(item.text),
                    // ignore: avoid_print
                    onTap: () {
                      setState(() {
                        if (item.color == Colors.red) {
                          item.color = Colors.blue;
                        } else {
                          item.color = Colors.red;
                        }
                        // change the color to red on tap
                      });
                    },
                  ),
                );
              },
            ),
          ),

          const Divider(
            color: Colors.black,
          ),
          //refresh button
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.orange,
            ),
            onPressed: () {
              setState(() {
                items = items
                    .map((item) => Tile(text: item.text, color: Colors.blue))
                    .toList();
              });
            },
            child: const Text('Clear All'),
          ),
        ],
      ),
    );
  }
}
